# 문제 4-7
# 아래와 같은 패턴의 별(*)을 출력하는 프로그램을 작성해 보세요.

#     *
#    ***
#   *****
#  *******
# *********

def question_4_7(line_num=5):
    """ question 4-7 """
    space = ' '
    star = '*'
    for i in range(1, line_num + 1):
        side_space = (line_num - i) * space
        stars = ((i * 2) - 1) * star
        print(side_space + stars + side_space)
    print()


print(question_4_7.__doc__)
question_4_7()


# 문제 4-8
# 아래와 같은 패턴의 별(*)을 출력하는 프로그램을 작성 해보세요.

# *********
#  *******
#   *****
#    ***
#     *

def question_4_8(line_num=5):
    """ question 4-8 """
    space = ' '
    star = '*'
    for i in range(line_num, 0, -1):
        side_space = (line_num - i) * space
        stars = ((i * 2) - 1) * star
        print(side_space + stars + side_space)
    print()


print(question_4_8.__doc__)
question_4_8()

# 문제 4-9
# 중첩 루프를 이용해 신문 배달을 하는 프로그램을 작성하세요. 단, 아래에서 arrears 리스트는 신문 구독료가 미납된 세대에 대한 정보를 포함하고 있는데, 해당 세대에는 신문을 배달하지 않아야 합니다.

# >>> aparts = [[101, 102, 103, 104], [201, 202, 203, 204], [301, 302, 303, 304], [401, 402, 403, 404]]
# >>> arrears = [101, 203, 301, 404]
# >>>
print("question_4_9")
aparts = [[101, 102, 103, 104], [201, 202, 203, 204],
          [301, 302, 303, 304], [401, 402, 403, 404]]
arrears = [101, 203, 301, 404]

result = [num for apart in aparts for num in apart if num not in arrears]
print(result)
