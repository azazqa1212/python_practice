import os

# 문제 5-1
# 두 개의 정수 값을 받아 두 값의 평균을 구하는 함수를 작성하세요.

# def myaverage(a, b):
#     # 함수 구현


def myaverage(a, b):
    """ question 5-1 """
    return (a + b)/2


print(myaverage.__doc__)
print(myaverage(1, 0))
print()

# 문제 5-2
# 함수의 인자로 리스트를 받은 후 리스트 내에 있는 모든 정수 값에 대한 최댓값과 최솟값을 반환하는 함수를 작성하세요.

# def get_max_min(data_list):
#     # 함수 구현


def get_max_min(data_list):
    """ question 5-2 """
    max_num = max(data_list)
    min_num = min(data_list)
    return (max_num, min_num)


print(get_max_min.__doc__)
print(get_max_min(range(10)))
print()

# 문제 5-3
# 절대 경로를 입력받은 후 해당 경로에 있는 *.txt 파일의 목록을 파이썬 리스트로 반환하는 함수를 작성하세요.

# def get_txt_list(path):
#     # 함수 구현


def get_txt_list(path):
    """ question 5-3 """
    if os.path.isdir(path):
        for x in os.listdir(path):
            if x.endswith('txt'):
                print(x)


testdir = "/home/azazqa/workspace/python/python-practice/testdir"
print(get_txt_list.__doc__)
get_txt_list(testdir)
print()


# 문제 5-4
# 체질량 지수(BMI; Body Mass Index)는 인간의 비만도를 나타내는 지수로서 체중과 키의 관계로 아래의 수식을 통해 계산합니다.
# 여기서 중요한 점은 체중의 단위는 킬로그램(kg)이고 신장의 단위는 미터(m)라는 점입니다.

# BMI=체중(kg)신장(m)2
# 일반적으로 BMI 값에 따라 다음과 같이 체형을 분류하고 있습니다.

# BMI <18.5, 마른체형
# 18.5 <= BMI < 25.0, 표준
# 25.0 <= BMI < 30.0, 비만
# BMI >= 30.0, 고도 비만
# 함수의 인자로 체중(kg)과 신장(cm)을 받은 후 BMI 값에 따라 ‘마른체형’, ‘표준’, ‘비만’, ‘고도 비만’ 중 하나를 출력하는 함수를 작성하세요.

def get_bmi(weight, stature):
    """ question 5-4 """
    try:
        bmi = weight / ((stature/100) ** 2)
        result = ""
        if bmi >= 30:
            result = "고도 비만"
        elif bmi >= 25:
            result = "비만"
        elif bmi >= 18:
            result = "표준"
        else:
            result = "마른 체형"
        return result
    except:
        pass


print(get_bmi.__doc__)
print(get_bmi(60, 180))
print()


# 문제 5-5
# 사용자로부터 키(cm)와 몸무게(kg)를 입력받은 후 BMI 값과 BMI 값에 따른 체형 정보를 화면에 출력하는 프로그램을 작성해 보세요.
# 파이썬에서 사용자 입력을 받을 때는 input 함수를 사용하며,
# 작성된 프로그램은 계속해서 사용자로부터 키와 몸무게를 입력받은 후 BMI 및 체형 정보를 출력해야 합니다(무한 루프 구조).
def get_bmi_input():
    """ question 5-5 """
    # 2번만 반복
    count = 0
    while count < 2:
        weight = input("plz input weight: \n")
        stature = input("plz input stature: \n")
        print(get_bmi(int(weight), int(stature)))
        count += 1


print(get_bmi_input.__doc__)
# get_bmi_input()
print()


# 문제 5-7
# 함수의 인자로 시작과 끝을 나타내는 숫자를 받아 시작부터 끝까지의 모든 정수값의 합을 반환하는 함수를 작성하세요(시작값과 끝값을 포함).

# def add_start_to_end(start, end):
#     # 함수 구현

def add_start_to_end(start, end):
    """ question 5-7 """
    sum = 0
    for num in range(start, end+1):
        sum += num
    return sum


print(add_start_to_end.__doc__)
print(add_start_to_end(5, 11))
print()

# 문제 5-8
# 함수의 인자로 문자열을 포함하는 리스트가 입력될 때 각 문자열의 첫 세 글자로만 구성된 리스트를 반환하는 함수를 작성하세요.
# 예를 들어, 함수의 입력으로 ['Seoul', 'Daegu', 'Kwangju', 'Jeju']가 입력될 때 함수의 반환값은 ['Seo', 'Dae', 'Kwa', 'Jej']입니다.


def cut_strings(args):
    """ question 5-8 """
    cuted_list = [item[:3] for item in args]
    return cuted_list


print(cut_strings.__doc__)
strings = ['Seoul', 'Daegu', 'Kwangju', 'Jeju']
print(cut_strings(strings))
print()
